from django.shortcuts import render
from blogapp.models import BlogModel, TagModel
from django.contrib.auth.models import User

# us1 = User.objects.all()
# print(us1)

# tag = TagModel.objects.first()
# print(tag.blogs.first().title)

# blog = BlogModel.objects.get(id=3)
# print(blog.tag.first())

# # ----------------------------------------------------------
# # # 關係對像的綁定與解綁
# blog = BlogModel.objects.first()
# print(blog)
# tags = TagModel.objects.all()
# print(tags)

# # 綁定
# blog.tag.add(tags[1])
# print(blog.tag.all())

# # 清空
# blog.tag.clear()
# print(blog.tag.all())

# for tag in tags:
#     blog.tag.add(tag)
# print(blog.tag.all())

# blog.tag.remove(tags[3])
# print(blog.tag.all())

# tg= blog.tag.all()[2]
# blog.tag.remove(tg)
# print(blog.tag.all())

# # 多對一的綁定與解綁
# print(blog.user)
# ad = User.objects.last()
# blog.user = ad
# blog.save()
# print(blog.user)
# blogs = BlogModel.objects.all()

# #從tag到blog (多對多)
# tag = TagModel.objects.first()
# blogs = tag.blogs.all()
# print(blogs)

# tag.blogs.remove(blogs[0])
# print(tag.blogs.all())

# # -----------------基礎查詢---get/filter---------------
# # get函數，獲取單個對象，如果獲取不到，報錯
# print(TagModel.objects.get(name="標籤1"))
# # TagModel.objects.get(name="python")報錯

# # filter函數，獲取查詢的結果集，如果獲取不到返回空的結果集
# user = User.objects.first()
# print(user)

# print(user.blogs.all()) # 第一種
# print(BlogModel.objects.filter(user=user)) # 第二種

# user1 = User.objects.get(username='user1')
# print(user1)
# print(BlogModel.objects.filter(user=user1))

# # 獲取出全部blog對象的id
# blogs = BlogModel.objects.all()
# for blog in blogs:
#     print(blog.id, blog)

# print(BlogModel.objects.filter(id__lt=3)) # __lt: 小於 __gte:大於等於  __lte:小於等於

# # -----------------三種方式解決get報錯問題--------------------------------------------------------
from django.shortcuts import HttpResponse, get_object_or_404
from .models import BlogModel
# # 網站404狀態html可以更改
def demo(request, title):
#     # 方案一 (推薦)
    blog = get_object_or_404(BlogModel, title=title)
    
#     # 方案二: 
#     # try:
#     #     blog = BlogModel.objects.get(title=title)
#     # except:
#     #     return HttpResponse('', status=404)

#     # 方案三:(不夠優雅)
#     # blogs = BlogModel.objects.filter(title=title)
#     # if blogs.count() < 1:
#     #     return HttpResponse('', status=404)
#     # else:
#     #     blog = blogs.first()
    return HttpResponse(blog.title, status=200)


# # ------------------------------------lt/lte/gt/gte四個參數查詢的不同------------------------------------------
# users = User.objects.all()
# for i in users:
#     print(i.id, i.username, i.email)

# print(User.objects.filter(id=1)) 
# print(User.objects.filter(id__lt=5)) #小於
# print(User.objects.filter(id__lte=5)) #小於等於
# print(User.objects.filter(id__gt=5)) #大於
# print(User.objects.filter(id__gte=5)) #大於等於

# # User.objects.get(id__lt=5) 返回的對象大於1個
# # User.objects.get(id__lt=4) 返回的對象等於1個，成功
# print(get_object_or_404(User, id__lt=4)) # 注意返回結果


# #-------------------(查詢參數)-contains和icontains的使用-----------------------------
# # contains: %xxxxxx%
# print(TagModel.objects.filter(name='標籤2'))
# print(TagModel.objects.filter(name__contains='標籤2'))
# print(TagModel.objects.filter(name__contains='blog'))
# print(TagModel.objects.filter(name__icontains='blog'))
# # contains: 檢測包含，區分大小寫
# # icontains: 檢測包含，不區分大小寫
# # ps: 但是django默認的數據庫是sqlite3，sqlite數據庫部區分大小寫
# # 所以在django使用默認的sqllite3的時候，contains與icontains是沒有任何不同的


#  #-------------------(查詢參數)-exact和iexact的使用-----------------------------
# # exact: xxxxxx
# # 精確匹配
# # exact:區分大小寫
# # iexact|: 不區分大小寫
# print(TagModel.objects.filter(name='Python'))
# print(TagModel.objects.filter(name__exact='Python'))
# print(TagModel.objects.filter(name__iexact='Python'))


# #  #-------------------(查詢參數)-[i]startswith和[i]endswith的使用-----------------------------
# # startswith: 起始匹配，區分大小寫
# # istartswith:起始匹配，不區分大小寫
# # endswith:末尾匹配，區分大小寫
# # iendswith:末尾匹配，不區分大小謝
# # startswith, endswith與contains很像
# # 只是判斷位置不同，所以他們在sqllite3數據庫裡是無法判別大小寫字符
# print(TagModel.objects.filter(name__startswith='blog'))
# print(TagModel.objects.filter(name__istartswith='blog'))
# print(TagModel.objects.filter(name__endswith='on'))
# print(TagModel.objects.filter(name__iendswith='on'))

# # -------------------------(查詢參數)---in和range----------------------------------
# tags = TagModel.objects.all()
# for tag in tags:
#     print(tag.id, tag.name)

# # in 在某個範圍內
# intags = TagModel.objects.filter(id__in=[20019,20021,20025])
# print(intags)
# for tag in intags:
#     print(tag.id, tag.name)


# for i in range(1, 1000): # 左閉右開
#     print(i, end=' ')
# # manytags = TagModel.objects.filter(id__in=range(1,300000)) #Error: too many SQL variables
# # for tag in manytags:                                       #對in來說10000有點多，將id放到range裡面去查找，這是一個循環    
# #     print(tag.id, tag.name)

# manytags = TagModel.objects.filter(id__range=[20016, 20020]) # 左閉右閉，包含左右兩個數字
# for tag in manytags:                                         
#     print(tag.id, tag.name)
# manytags = TagModel.objects.filter(id__range=[1, 100000]) # 左閉右閉，包含左右兩個數字
# for tag in manytags:                                      # 從1到10萬的數字，速度挺快   
#     print(tag.id, tag.name)                               # 將id拿到1~100000進行比大小，在他們之間就是成立的，這是一個判斷操作


# --------------------------(查詢參數)------isnull---關係和字段的不同----------------------------------
# blogs = BlogModel.objects.filter(tag__isnull=False)
# for blog in blogs:
#     print(blog.title)
# blogs = BlogModel.objects.filter(tag__isnull=True) # 查詢blogapp_blogmodel_tag表
# for blog in blogs:
#     print(blog.id, blog.title)

# blogs = BlogModel.objects.filter(user__isnull=True) # 通過blog查詢user 
# for blog in blogs:
#     print(blog.id, blog.title)

# blogs = BlogModel.objects.filter(user__isnull=False) 
# for blog in blogs:
#     print(blog.id, blog.title)

# users = User.objects.filter(blogs__isnull=True) # 通過user查詢blog
# for user in users:
#     print(user.id, user.username)

# users = User.objects.filter(blogs__isnull=False) # 通過user查詢blog
# for user in users:
#     print(user.id, user.username)

# tags = TagModel.objects.filter(blogs__isnull=True)  # 通過標籤查詢博客是否為null
# for tag in tags:
#     print(tag.name)

# tags = TagModel.objects.filter(blogs__isnull=False)  # 通過標籤查詢博客是否為null
# for tag in tags:
#     print(tag.name)


# ----------------------批量創建博客模型-----------------------------
# import random, faker
# from django.utils.timezone import now
# import datetime
# fake =faker.Faker()
# for _ in range(1, 10):
#     print(now()-datetime.timedelta(days=random.randint(1,365*3)))

# users = User.objects.all()
# for _ in users:
#     print(random.choices(users))

# for _ in range(1, 6):
#     print(fake.name())

# for _ in range(1, 6):
#     print(fake.text())

# blog_objects = []
# for _ in range(1, 1000):
#     blog = BlogModel()
#     blog.title = fake.name()
#     blog.content = fake.text()
#     blog.read_number = 1000
# # 創建時間
#     blog.create_time = now() - datetime.timedelta(days=random.randint(1,365*3))
#     blog.user = random.choice(users)
#     blog_objects.append(blog)
# BlogModel.objects.bulk_create(blog_objects)

# for _ in range(1, 1000):
#     BlogModel.objects.create(
#         title = fake.name(),
#         content = fake.text(),
#         read_number = 1000,
#         user = random.choice(users),
#         )
# BlogModel.objects.all().delete()
# BlogModel.objects.filter(read_number=1000).delete()    


# # 多對多的關係創建，必須是對象已經存在數據庫內
# # for blog in blog_objects: 
# #     print(blog)

# tags = TagModel.objects.all()
# # for _ in range(1, 10):
# #     print(random.choices(tags, k=2))

# for blog in BlogModel.objects.filter(read_number=1000):
#     # blog.tag.add(tags[1]) # 如果是一個對象，直接用add函數
#     blog.tag.add(*random.choices(tags,k=2)) # 如果是多個對象，做成列表，用add 函數，並在列表前加*
#     blog.save()
#     # print(blog.tag.count())
#     print(blog.tag.all())
# # 說明 random.choices可以重複提取，所以可能添加了一個對象



# #  ------------------       日期查詢: 通過年、月、日查詢----------------------
# print(BlogModel.objects.filter(create_time__year=2023).count())

# for i in range(2021,2025):
#     blogs = BlogModel.objects.filter(create_time__year=i)#通過年份查詢
#     print('{}年有{}條數據'.format(i, blogs.count()))


# print(BlogModel.objects.filter(create_time__month=3).count()) #通過月份查詢

# total = 0
# for i in range(2021, 2025):
#     blogs = BlogModel.objects.filter(create_time__year=i)
#     total += blogs.count()
#     print('{}年有{}條數據'.format(i, blogs.count()))
# print('總數巨量: {}'.format(total))

# total = 0
# for i in range(1, 13):
#     blogs = BlogModel.objects.filter(create_time__month=i)
#     total += blogs.count()
#     print('{}月有{}條數據'.format(i, blogs.count()))
# print('總數據量: {}'.format(total))

# print(BlogModel.objects.filter(create_time__day=20).count()) #通過天查詢
# total = 0
# for i in range(1, 32):
#     blogs = BlogModel.objects.filter(create_time__day=i)
#     total += blogs.count()
#     print('{}日有{}條數據'.format(i, blogs.count()))
# print('總數據量: {}'.format(total))

# blog_2023 = BlogModel.objects.filter(create_time__year=2023)# 通過年份和月份來查詢數據
# print(blog_2023.count())

# blog_2023_3 = blog_2023.filter(create_time__month=3)
# print(blog_2023_3.count())

# for year in range(2020, 2025):
#     year_blogs = BlogModel.objects.filter(create_time__year=year)
#     print('{}年有{}條數據'.format(year, year_blogs.count()))
#     for m in range(1, 13):
#         mblogs = year_blogs.filter(create_time__month=m)
#         print('    {}年{}月有{}條數據'.format(year, m , mblogs.count()))        

# # #  ------------------       日期查詢: 按時、分、秒----------------------
# for i in range(0, 24):
#     print('{}時有{}條數據'.format(i, BlogModel.objects.filter(create_time__hour=i).count()))
# for i in range(0, 60):
#     print('{}分有{}條數據'.format(i, BlogModel.objects.filter(create_time__minute=i).count()))
# for i in range(0, 60):
#     print('{}秒有{}條數據'.format(i, BlogModel.objects.filter(create_time__second=i).count()))


# # # #  ------------------       日期查詢: 按時、分、秒--------------------- -
# import datetime
# from django.utils.timezone import now
# # 時間範圍查詢
# t1 =now()
# print(t1)

# t2 = now() - datetime.timedelta(days=10)
# print(t2)

# print(BlogModel.objects.filter(create_time__range=[t2,t1]).count())

# for i in range(1,11):
#     print('從{}到{}時間內，有{}條紀錄'.format(t2, t1, BlogModel.objects.filter(create_time__range=[t2, t1]).count()))
#     t1 =t2
#     t2 = t1-datetime.timedelta(days=10)

# #數字範圍
# for i in range(2020, 2025):
#     print(i, BlogModel.objects.filter(create_time__year=i).count())

# print(BlogModel.objects.filter(create_time__year__range=[2020, 2023]).count()) # 年份範圍
# print(BlogModel.objects.filter(create_time__year__in=[2020, 2023]).count())   #指定年分

## -----------------------------------(查詢參數)     正則匹配-----------------------------------------------
# regex 正則就是亮點
# print(BlogModel.objects.filter(create_time__year__regex='\d+3').count()) # \d 數字    + 一個或者1個以上
# print(BlogModel.objects.filter(create_time__year__regex='20\d+3').count()) # \d 數字    + 一個或者1個以上
# blogs = BlogModel.objects.filter(title__regex='Michelle.*?')        # .*? 非貪婪模式的匹配字符串  .*貪婪模式下的匹配字符串
# for blog in blogs:                                                  # regex區分大小寫
#     print(blog.title)                                               # iregex不區分大小寫                 

### 常用正則
# \d 數字
# + 一個或一個以上
# [4] 固定四個
# [a-zA-Z] 匹配從a到z ,A到Z中任意字符
# .*? 非貪婪模式
# .* 貪婪模式


# #--------------(多條件聯合)--------------Model Q-查詢---------------------------------
# from django.db.models import Q
# # 查詢2022年和2023年 2月份的數據量
# #方案一: 最low
# blog_2022 = BlogModel.objects.filter(create_time__year=2022)
# blog_2023 = BlogModel.objects.filter(create_time__year=2023)
# print(blog_2022.count(), blog_2023.count())

# print(blog_2022.filter(create_time__month=2).count())
# print(blog_2023.filter(create_time__month=2).count())

# # 方案二: 還行
# blogs = BlogModel.objects.filter(create_time__year__in=[2022,2023])
# print(blogs.count())
# print(blogs.filter(create_time__month=2).count())

# # 方案三: 還行
# blogs = BlogModel.objects.filter(create_time__year__range=[2022,2023])
# print(blogs.count())
# print(blogs.filter(create_time__month=2).count())

# #方案四: 推薦，因為高大上
# q = Q(create_time__year__in=[2022,2023])
# blogs = BlogModel.objects.filter(q)
# print(blogs.count())

# q1 = Q(create_time__year__in=[2022,2023])
# q2 = Q(create_time__month=2)
# blogs =BlogModel.objects.filter(q1, q2) # 與
# print(blogs.count())

# blogs= BlogModel.objects.filter(q1 | q2)  #或 ，查詢符合q1或者q2條件的對象
# print(blogs.count())

# for i in range(1,13):
#     print('2017年{}月份，{}條數據'.format(i, BlogModel.objects.filter(Q(create_time__year=2023), Q(create_time__month=i)).count()))



# # --------------------------------使用Q的與、非模式查詢---------------------------------------------
# from django.db.models import Q
# q0 = Q(create_time__year=2021)
# q1 = Q(create_time__year=2022)
# q2 = Q(create_time__year=2023)
# q3 = Q(create_time__month=3)
# q4 = Q(create_time__month=7)

# #與 查詢出21 22 23年，3月或者7月的數據總量
# print(BlogModel.objects.filter(q1|q2|q0, q3|q4).count())

# blogs = BlogModel.objects.filter(create_time__year__range=[2021,2023])
# blog = blogs.filter(create_time__month__in=[3, 7])
# blo = blogs.filter(create_time__month__in=[1,2,4,5,6,8,9,10,11,12])

# print(blogs.count(), blog.count(),blo.count())
# print(blogs.filter(create_time__month=3).count(), blogs.filter(create_time__month=7).count())

# # 非     ~
# # 查詢出21,22,23三年中，去掉3月和7月份的數據總量
# print(BlogModel.objects.filter(q0|q1|q2, ~(q3|q4)).count())
# print(BlogModel.objects.filter(q0|q1|q2, ~q3, ~q4).count())