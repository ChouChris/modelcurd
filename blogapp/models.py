from django.db import models
from django.utils.timezone import now
from django.contrib.auth.models import User
# Create your models here.

class TagModel(models.Model):
    name = models.CharField(max_length=32, verbose_name='標籤名')
    create_time = models.DateTimeField(default=now, verbose_name='創建時間')

    def __str__(self):
        return  self.name
    
    class Meta:
        verbose_name='標籤'
        verbose_name_plural = '標籤模型'

class BlogModel(models.Model):
    title = models.CharField(max_length=64, verbose_name="標題")
    content = models.TextField(verbose_name='博客正文')
    create_time = models.DateTimeField(default=now, verbose_name='創建時間')
    read_number = models.IntegerField(default=1, verbose_name='閱讀量')
    user = models.ForeignKey(to=User, related_name='blogs', verbose_name='用戶', on_delete=models.CASCADE)
    tag = models.ManyToManyField(to=TagModel, related_name='blogs',verbose_name='標籤名')
    def __str__(self):
        return '<<{}:{}>>標籤: {}'.format(self.title, self.user.username, [tag for tag in self.tag.all()])
    
    class Meta:
        verbose_name='博客'
        verbose_name_plural = '博客模型'
    

