from django.contrib import admin
from .models import TagModel,BlogModel
# Register your models here.
admin.site.register(TagModel)
admin.site.register(BlogModel)